package docongbang.com.money_converter;

import docongbang.com.money_converter.amount.AmountTextField;
import docongbang.com.money_converter.direction.DirectionComboBox;
import docongbang.com.money_converter.main.MainAgent;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    public static void main( String[] args )
    {
    	launch(args);
    }

	@Override
	public void start(Stage primaryStage) throws Exception {
		MainAgent mainAgent = new MainAgent();
		
		AmountTextField amountInput = new AmountTextField(true);
		amountInput.getView().getTextField().addEventHandler(KeyEvent.KEY_RELEASED, new NumberInputHandler(amountInput.getView()));
		
		AmountTextField amountOutput = new AmountTextField(false);
		
		DirectionComboBox directionComboBox = new DirectionComboBox();
		directionComboBox.getView().getComboBox().addEventHandler(ActionEvent.ANY, new SelectDirectionHandler(directionComboBox.getView()));
		
		amountInput.getModel().addObserver(mainAgent.getModel());
		mainAgent.getModel().addObserver(amountInput.getModel());
		
		amountOutput.getModel().addObserver(mainAgent.getModel());
		mainAgent.getModel().addObserver(amountOutput.getModel());
		
		directionComboBox.getModel().addObserver(mainAgent.getModel());
		mainAgent.getModel().addObserver(directionComboBox.getModel());
		
		BorderPane root = new BorderPane();
		
		Label appLabel = new Label("Euro - Dollar Converter");
		root.setTop(appLabel);
		BorderPane.setAlignment(appLabel, Pos.CENTER);
		BorderPane.setMargin(appLabel, new Insets(0, 0, 20, 0));
		
		GridPane mainContainer = new GridPane();
		mainContainer.setHgap(20d);
		
		VBox inputSide = new VBox();
		Label inputLabel = new Label("Amount Input");
		inputSide.getChildren().addAll(inputLabel, amountInput.getView().getTextField());
		inputSide.setAlignment(Pos.CENTER);
		mainContainer.add(inputSide, 0, 0);
		
		VBox convertSide = new VBox();
		convertSide.setAlignment(Pos.CENTER);
		convertSide.getChildren().add(directionComboBox.getView().getComboBox());
		mainContainer.add(convertSide, 1, 0);
		
		VBox outputSide = new VBox();
		outputSide.setAlignment(Pos.CENTER);
		Label outputLabel = new Label("Amount Output");
		outputSide.getChildren().addAll(outputLabel, amountOutput.getView().getTextField());
		mainContainer.add(outputSide, 2, 0);
		
		root.setCenter(mainContainer);
		BorderPane.setMargin(mainContainer, new Insets(0, 0, 20, 0));
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);		
		primaryStage.setTitle("Euro - Dollar Converter");
		primaryStage.show();
	}
}