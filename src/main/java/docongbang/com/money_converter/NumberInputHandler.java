package docongbang.com.money_converter;

import docongbang.com.money_converter.amount.ViewAmount;
import docongbang.com.money_converter.core.EMessageTitle;
import docongbang.com.money_converter.core.MessageMVC;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class NumberInputHandler implements EventHandler<KeyEvent> {
	private ViewAmount view;
	
	public NumberInputHandler(ViewAmount viewAmount) {
		this.view = viewAmount;
	}
	
	public void handle(KeyEvent event) {
		if (event.getCode().isDigitKey() || event.getCode().equals(KeyCode.PERIOD)) {
			System.out.println(view.toString() + " was pressed with NUMBER | PERIOD, notify observers");
//			view.notifyObservers(view.getTextField().getText());
			MessageMVC msg = new MessageMVC(EMessageTitle.INPUT_CHANGES);
			view.notifyObservers(msg);
		}
		
		if (event.getCode().equals(KeyCode.ESCAPE)) {
			System.out.println(view.toString() + " was pressed with ESCAPE, notify observers");
			view.getTextField().setText("");
			MessageMVC msg = new MessageMVC(EMessageTitle.INPUT_EMPTY);
			view.notifyObservers(msg);
		}
	}
	
}