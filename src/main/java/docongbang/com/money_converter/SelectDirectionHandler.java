package docongbang.com.money_converter;

import docongbang.com.money_converter.core.EMessageTitle;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.direction.ViewDirection;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SelectDirectionHandler implements EventHandler<ActionEvent> {
	
	private ViewDirection view;
	
	public SelectDirectionHandler(ViewDirection viewDirection) {
		this.view = viewDirection;
	}
	
	public void handle(ActionEvent event) {
		MessageMVC msg = new MessageMVC(EMessageTitle.CHANGE_SELECT);
		view.notifyObservers(msg);
	}		
}