package docongbang.com.money_converter.amount;

public class AmountTextField {
	
	private ViewAmount view;
	private ModelAmount model;
	private ControlAmount control;
	
	public AmountTextField(boolean ediable) {
		view = new ViewAmount();
		view.getTextField().setEditable(ediable);
		control = new ControlAmount();
		model = new ModelAmount();
		view.addObserver(control);
		control.addObserver(model);
		control.addObserver(view);
		model.addObserver(view);
	}
	
	public ControlAmount getControl() {
		return control;
	}
	
	public ViewAmount getView() {
		return view;
	}
	
	public ModelAmount getModel() {
		return model;
	}
}
