package docongbang.com.money_converter.amount;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.IViewObserver;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.core.ViewObservable;

public class ControlAmount extends ControlObservable implements IViewObserver {
	
	public ControlAmount() {
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}

	public void update(ViewObservable viewObservable, Object message) {
		ViewAmount viewAmount = (ViewAmount) viewObservable;
		MessageMVC messageMVC = (MessageMVC) message;
		try {
			float newAmount = Float.parseFloat(viewAmount.getTextField().getText());
			System.out.println(this.toString() + " is notified by " + viewObservable.toString() + ", convert text to float, then notify ModelAmount observers with " + newAmount);
			messageMVC.setValue(new Float(newAmount));
			notifyObservers(messageMVC);
		} catch (NumberFormatException e) {
			System.out.println(this.toString() + " is notified by " + viewObservable.toString() + ", fail to convert to float, then notify ModelAmount observers with 0.0f");
			messageMVC.setValue(new Float(0.0f));
			notifyObservers(messageMVC);
		}
	}
}
