package docongbang.com.money_converter.amount;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.EMessageTitle;
import docongbang.com.money_converter.core.IControlObserver;
import docongbang.com.money_converter.core.IModelObserver;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.core.ModelObservable;
import docongbang.com.money_converter.main.ModelMain;

public class ModelAmount extends ModelObservable implements IControlObserver, IModelObserver {
	private float amount;
	
	public ModelAmount() {
	}
	
	public float getAmount() {
		return amount;
	}

	public void update(ControlObservable controlObservable, Object message) {
		MessageMVC messageMVC = (MessageMVC) message;
		System.out.println(this.toString() + " is notified by " + controlObservable.toString() + " with message: " + messageMVC.toString());
		if (messageMVC.getTitle().equals(EMessageTitle.INPUT_CHANGES)) {
			Float newAmount = (Float) messageMVC.getValue();
			amount = newAmount.floatValue();
			
		}
		notifyObserversModel(messageMVC);
	}
	
	public void update(ModelObservable modelObservable, Object message) {
		if (modelObservable instanceof ModelMain ) {
			System.out.println(this.toString() + " is notified by " + modelObservable.toString() + " with message: " + message.toString());
		}
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
