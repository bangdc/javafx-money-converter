package docongbang.com.money_converter.amount;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.IControlObserver;
import docongbang.com.money_converter.core.IModelObserver;
import docongbang.com.money_converter.core.ModelObservable;
import docongbang.com.money_converter.core.ViewObservable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class ViewAmount extends ViewObservable implements IModelObserver, IControlObserver {
	private TextField textField;
	
	/**
	 * Create new instance of ViewAmount
	 */
	public ViewAmount() {
		textField = new TextField();
//		textField.addEventHandler(KeyEvent.KEY_RELEASED, new NumberInputHandler(this));
	}
	
	/**
	 * 
	 * @return
	 */
	public TextField getTextField() {
		return textField;
	}
	
	/**
	 * 
	 */
	public void update(ModelObservable modelObservable, Object message) {
		System.out.println("Text Field Get New Update From Model");
		Float newAmount = (Float) message;
		textField.setText(newAmount.toString());
	}

	/**
	 * 
	 */
	public void update(ControlObservable controlObservable, Object message) {
		
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
