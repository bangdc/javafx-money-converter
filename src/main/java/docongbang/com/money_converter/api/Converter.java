package docongbang.com.money_converter.api;

public class Converter {
	private float rateEurToUsd = 1.07f;
	
	public float getRateEurToUsd() {
		return rateEurToUsd;
	}
	
	public float getEurFromUsd(float usdAmount) {
		return usdAmount / getRateEurToUsd();
	}
	
	public float getUsdFromEur(float eurAmount) {
		return eurAmount * getRateEurToUsd();
	}
}
