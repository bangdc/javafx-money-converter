package docongbang.com.money_converter.core;

import java.util.ArrayList;

public abstract class ControlObservable {
	private ArrayList<IControlObserver> observers;
		
	public ControlObservable() {
		observers = new ArrayList<IControlObserver>();
	}
	
	public void addObserver(IControlObserver observer) {
		observers.add(observer);
	}
	
	public void removeObserver(IControlObserver observer) {
		observers.remove(observer);
	}
	
	public void notifyObserver(IControlObserver observer, Object message) {
		observer.update(this, message);
	}
	
	public void notifyObservers(Object message) {
		for (IControlObserver iControlObserver : observers) {
			notifyObserver(iControlObserver, message);
		}
	}
} 
