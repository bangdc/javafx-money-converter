package docongbang.com.money_converter.core;

public enum EMessageTitle {
	INPUT_CHANGES,
	INPUT_EMPTY,
	ENABLE_SELECT,
	DISABLE_SELECT,
	CHANGE_SELECT,
	CALCULATE_OUTPUT,
	NEW_OUTPUT
}
