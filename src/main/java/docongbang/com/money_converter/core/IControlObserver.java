package docongbang.com.money_converter.core;

public interface IControlObserver {
	public void update(ControlObservable controlObservable, Object message);
}
