package docongbang.com.money_converter.core;

public interface IModelObserver {
	public void update(ModelObservable modelObservable, Object message);
}
