package docongbang.com.money_converter.core;

public interface IViewObserver {
	public void update(ViewObservable viewObservable, Object message);
}
