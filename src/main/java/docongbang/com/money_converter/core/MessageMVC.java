package docongbang.com.money_converter.core;

public class MessageMVC {
	private Object value;
	private Object sender;
	private Object receiver;
	private EMessageTitle title;
	
	public MessageMVC(EMessageTitle title) {
		this.title = title;
	}
	
	public MessageMVC(EMessageTitle title, Object value) {
		this.value = value;
		this.title = title;
	}
	
	public MessageMVC(EMessageTitle title, Object value, Object sender, Object receiver) {
		this.value = value;
		this.title = title;
		this.sender = sender;
		this.receiver = receiver;
	}
	
	public void setReceiver(Object receiver) {
		this.receiver = receiver;
	}
	
	public void setSender(Object sender) {
		this.sender = sender;
	}
	
	public void setTitle(EMessageTitle title) {
		this.title = title;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public Object getReceiver() {
		return receiver;
	}
	
	public Object getValue() {
		return value;
	}
	
	public Object getSender() {
		return sender;
	}
	
	public EMessageTitle getTitle() {
		return title;
	}
	
	@Override
	public String toString() {
		StringBuilder myString = new StringBuilder();
//		return "From " + sender.toString() + " to " + receiver.toString() + " title " + title.toString() + " value " + value.toString();
		if (sender != null) {
			myString.append("From " + sender.toString());
		}
		
		if (receiver != null) {
			myString.append(" to " + receiver.toString());
		}
		
		myString.append(" title " + title.toString());
		
		if (value != null) {
			myString.append(" value " + value.toString());
		}
		
		return myString.toString();
	}
}
