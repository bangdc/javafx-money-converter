package docongbang.com.money_converter.core;

import java.util.ArrayList;

public abstract class ModelObservable {
	private ArrayList<IModelObserver> observers;
	
	public ModelObservable() {
		observers = new ArrayList<IModelObserver>();
	}
	
	public void addObserver(IModelObserver observer) {
		observers.add(observer);
	}
	
	public void removeObserver(IModelObserver observer) {
		observers.remove(observer);
	}
	
	public void notifyObserver(IModelObserver observer, Object message) {
		observer.update(this, message);
	}
	
	public void notifyObservers(Object message) {
		for (IModelObserver iModelObserver : observers) {
			notifyObserver(iModelObserver, message);
		}
	}
	
	public void notifyObserversView(Object message) {
		for (IModelObserver iModelObserver : observers) {
			if (iModelObserver instanceof ViewObservable) {
				notifyObserver(iModelObserver, message);
			}
		}
	}
	
	public void notifyObserversModel(Object message) {
		for (IModelObserver iModelObserver : observers) {
			if (iModelObserver instanceof ModelObservable) {
				notifyObserver(iModelObserver, message);
			}
		}
	}
}
