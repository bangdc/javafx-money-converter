package docongbang.com.money_converter.core;

import java.util.ArrayList;

public abstract class ViewObservable {
	private ArrayList<IViewObserver> observers;
	
	public ViewObservable() {
		observers = new ArrayList<IViewObserver>();
	}
	
	public void addObserver(IViewObserver observer) {
		observers.add(observer);
	}
	
	public void removeObserver(IViewObserver observer) {
		observers.remove(observer);
	}
	
	public void notifyObserver(IViewObserver observer, Object message) {
		observer.update(this, message);
	}
	
	public void notifyObservers(Object message) {
		for (IViewObserver iViewObserver : observers) {
			notifyObserver(iViewObserver, message);
		}
	}
}
