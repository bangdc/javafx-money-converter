package docongbang.com.money_converter.direction;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.IViewObserver;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.core.ViewObservable;

public class ControlDirection extends ControlObservable implements IViewObserver {

	public ControlDirection() {
	}

	public void update(ViewObservable viewObservable, Object message) {
		ViewDirection viewDirection = (ViewDirection) viewObservable;
		MessageMVC messageMVC = (MessageMVC) message;
		System.out.println(this.toString() + " is notified by " + viewObservable.toString() + " with message: " + messageMVC.toString());
		messageMVC.setValue(viewDirection.getComboBox().getValue());
		notifyObservers(messageMVC);
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
