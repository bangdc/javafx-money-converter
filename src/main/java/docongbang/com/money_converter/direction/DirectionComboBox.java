package docongbang.com.money_converter.direction;

import java.util.Arrays;

public class DirectionComboBox {
	private ViewDirection view;
	private ControlDirection control;
	private ModelDirection model;
	
	public DirectionComboBox() {
		view = new ViewDirection(Arrays.asList("EUR => USD","USD => EUR"));
		control = new ControlDirection();
		model = new ModelDirection(false);
		
		model.addObserver(view);
		view.addObserver(control);
		control.addObserver(view);
		control.addObserver(model);
	}
	
	public ViewDirection getView() {
		return view;
	}
	
	public ControlDirection getControl() {
		return control;
	}
	
	public ModelDirection getModel() {
		return model;
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
