package docongbang.com.money_converter.direction;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.EMessageTitle;
import docongbang.com.money_converter.core.IControlObserver;
import docongbang.com.money_converter.core.IModelObserver;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.core.ModelObservable;
import docongbang.com.money_converter.main.ModelMain;

public class ModelDirection extends ModelObservable implements IControlObserver, IModelObserver {
	private boolean enable;
	
	public ModelDirection(boolean enable) {
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
	
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public void update(ControlObservable controlObservable, Object message) {
		MessageMVC messageMVC = (MessageMVC) message;
		
		if (messageMVC.getTitle().equals(EMessageTitle.CHANGE_SELECT)) {
			System.out.println(this.toString() + " is notified by " + controlObservable.toString() + " with message: " + messageMVC.toString());
			messageMVC.setTitle(EMessageTitle.CALCULATE_OUTPUT);
			notifyObserversModel(messageMVC);
		}
	}

	public void update(ModelObservable modelObservable, Object message) {
		if (modelObservable instanceof ModelMain) {
			System.out.println(this.toString() + " is notified by " + modelObservable.toString() + " with message: " + message.toString());
			notifyObserversView(message);
		}
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
