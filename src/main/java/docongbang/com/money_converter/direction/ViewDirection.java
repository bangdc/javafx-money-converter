package docongbang.com.money_converter.direction;

import java.util.List;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.EMessageTitle;
import docongbang.com.money_converter.core.IControlObserver;
import docongbang.com.money_converter.core.IModelObserver;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.core.ModelObservable;
import docongbang.com.money_converter.core.ViewObservable;
import javafx.scene.control.ComboBox;

public class ViewDirection extends ViewObservable implements IControlObserver, IModelObserver {
		
	private ComboBox<String> comboBox;
	
	public ViewDirection(List<String> list) {
		comboBox = new ComboBox<String>();
		comboBox.getItems().addAll(list);
		comboBox.setDisable(true);
	}
	
	public ComboBox<String> getComboBox() {
		return comboBox;
	}

	public void update(ModelObservable modelObservable, Object message) {
		MessageMVC messageMVC = (MessageMVC) message;
		System.out.println(this.toString() + " is notified by " + modelObservable.toString() + " with message: " + message.toString());
		if (messageMVC.getTitle() == EMessageTitle.ENABLE_SELECT) {
			comboBox.setDisable(false);
		}
		if (messageMVC.getTitle() == EMessageTitle.DISABLE_SELECT) {
			comboBox.setDisable(true);
		}
	}

	public void update(ControlObservable controlObservable, Object message) {
		
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}

}
