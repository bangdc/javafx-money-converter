package docongbang.com.money_converter.main;

import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.IViewObserver;
import docongbang.com.money_converter.core.ViewObservable;

public class ControlMain extends ControlObservable implements IViewObserver{
	
	public ControlMain() {
		
	}
	public void update(ViewObservable viewObservable, Object message) {
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
