package docongbang.com.money_converter.main;

public class MainAgent {
	private ViewMain view;
	private ControlMain control;
	private ModelMain model;
	
	public MainAgent() {
		view = new ViewMain();
		control = new ControlMain();
		model = new ModelMain();
		
		view.addObserver(control);
		control.addObserver(model);
		control.addObserver(view);
		model.addObserver(view);
	}
	
	public ControlMain getControl() {
		return control;
	}
	
	public ModelMain getModel() {
		return model;
	}
	
	public ViewMain getView() {
		return view;
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
