package docongbang.com.money_converter.main;

import docongbang.com.money_converter.amount.ModelAmount;
import docongbang.com.money_converter.api.Converter;
import docongbang.com.money_converter.core.ControlObservable;
import docongbang.com.money_converter.core.EMessageTitle;
import docongbang.com.money_converter.core.IControlObserver;
import docongbang.com.money_converter.core.IModelObserver;
import docongbang.com.money_converter.core.MessageMVC;
import docongbang.com.money_converter.core.ModelObservable;
import docongbang.com.money_converter.direction.ModelDirection;

public class ModelMain extends ModelObservable implements IControlObserver, IModelObserver{
	private Converter converter;
	private float input;
	
	public ModelMain() {
		converter = new Converter();
	}

	public void update(ModelObservable modelObservable, Object message) {
		
		MessageMVC messageMVC = (MessageMVC) message;
		
		if (modelObservable instanceof ModelAmount) {
			
			if (messageMVC.getTitle().equals(EMessageTitle.INPUT_CHANGES)) {
				System.out.println(this.toString() + " is notified by " + modelObservable.toString() + " with message: " + messageMVC.toString());
				this.input = (Float) messageMVC.getValue();
				messageMVC.setTitle(EMessageTitle.ENABLE_SELECT);
				messageMVC.setValue(null);
				notifyObserversModel(messageMVC);
			}
		}
		
		if (modelObservable instanceof ModelDirection) {
			if (messageMVC.getTitle().equals(EMessageTitle.CALCULATE_OUTPUT)) {
				System.out.println(this.toString() + " is notified by " + modelObservable.toString() + " with message: " + messageMVC.toString());
				String select = (String) messageMVC.getValue();
				
				if (select == "USD => EUR") {
					messageMVC.setTitle(EMessageTitle.NEW_OUTPUT);
					messageMVC.setValue(converter.getEurFromUsd(input));
					notifyObserversModel(messageMVC);
				} 
				
				if (select == "EUR => USD") {
					messageMVC.setTitle(EMessageTitle.NEW_OUTPUT);
					messageMVC.setValue(converter.getUsdFromEur(input));
					notifyObserversModel(messageMVC);
				}
			}
		}
	}

	public void update(ControlObservable controlObservable, Object message) {
		
	}
	
	@Override
	public String toString() {
		String oldString = super.toString();
		return oldString.substring(oldString.lastIndexOf(".") + 1);
	}
}
